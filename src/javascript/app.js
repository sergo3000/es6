import FightersView from './fightersView';
import { fighterService } from './services/fightersService';
import Fighter from './fighter';

class App {
  constructor() {
    this.startApp();
  }

  static rootElement = document.getElementById('root');
  static loadingElement = document.getElementById('loading-overlay');

  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';
      
      const fighters = await fighterService.getFighters();
      const fightersView = new FightersView(fighters);
      const fightersElement = fightersView.element;

      App.rootElement.appendChild(fightersElement);

      let fightButton = document.getElementById('fight-button')
      fightButton.addEventListener('click',async (event) => {
        let f1 = await fightersView.getExpandedFighter(Fighter.getIntRandom(1,6));
        let f2 = await fightersView.getExpandedFighter(Fighter.getIntRandom(1,6));
        
        //console.log(f1)
       this.fight(f1,f2)
      },false)

    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }
  fight(fighter1, fighter2){
    //fighter1 = new Fighter(fighter1)/////////////////////////////////////////////
   // fighter2 = new Fighter(fighter2)

    console.log(fighter1.health, fighter2.health)

    setTimeout(() => {

      let damage = fighter2.getHitPower() - fighter1.getBlockPower();
      fighter1.health -= Math.max(damage,0);
      //console.log(fighter1.health)
      if(this.checkWinner(fighter1,fighter2)){
        return;

      }
      damage = fighter1.getHitPower() - fighter2.getBlockPower();
      fighter2.health -= Math.max(damage,0);
      
      if(this.checkWinner(fighter1,fighter2)){
        return;

      }
      this.fight(fighter1, fighter2);
    },1000)
  }
  
  checkWinner(f1, f2){
    if(f1.isDead()){
      alert(`winner first fighter: ${f1.name}`)
      return f2;
    }
    else if(f2.isDead()){
      alert(`winner second fighter: ${f2.name}`)
      return f1;

    }
    return false;
  }
}

export default App;