import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import Fighter from './fighter';
import Modal from './modal'

class FightersView extends View {
  constructor(fighters) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
    
    this.modal = new Modal();
    this.modal.setCloseBehavior((fighter) => {
      this.fightersDetailsMap.set(fighter._id, fighter);
    })
  }

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  async handleFighterClick(event, fighter) {
    let expandedFighter = await this.getExpandedFighter(fighter._id)
    
    this.modal.open(expandedFighter);    
  }
  async getExpandedFighter(id){
    let expandedFighter;
    if(this.fightersDetailsMap.has(id)){ 
      expandedFighter = this.fightersDetailsMap.get(id)
    }else{
      expandedFighter = new Fighter(await fighterService.getFighterDetails(id))

      this.fightersDetailsMap.set(expandedFighter._id, expandedFighter);
      
    }
    return expandedFighter;
  }
}

export default FightersView;