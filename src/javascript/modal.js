import Fighter from "./fighter";
import { SIGTSTP } from "constants";

class Modal{
    constructor(){
        this.fighter //= new Fighter();
        this.form = document.getElementsByTagName('form')[0]
        this.closeFunc;
        this.modal = 
        this.init();
        
    }
    setCloseBehavior(func){
        this.closeFunc = func;
    }
    open(fighter){
        let modal = document.getElementById("myModal");
        this.fighter = fighter;
        
        this.form.elements.name.value   = fighter.name;
        this.form.elements.attack.value = fighter.attack;
        this.form.elements.defense.value= fighter.defense;
        this.form.elements.health.value = fighter.health;
        
        modal.style.display = "block";
    }
    init(){
        let modal = document.getElementById("myModal");
        window.document.addEventListener('click',(event) => {
            if (event.target == modal) {
            this.close()

            }
        },false)
        let span = document.getElementsByClassName("close")[0];
        span.addEventListener('click',() => {
            this.close();
        },false)
    }
    close(){
        let modal = document.getElementById("myModal");

        this.fighter.name = this.form.elements.name.value;   
        this.fighter.attack =this.form.elements.attack.value; 
        this.fighter.defense =this.form.elements.defense.value; 
        this.fighter.health = this.form.elements.health.value; 
        
        this.closeFunc(this.fighter);
        modal.style.display = "none";
    }
    getFighter(){
        return this.figher;
    }
}

export default Modal;