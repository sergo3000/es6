class Fighter{
    constructor({_id, source,name, health, attack, defense}) {
      this._id = _id;
      this.source =source;
      this.name = name;
      this.health = health;
      this.attack = attack;
      this.defense = defense;
      
    }
    getRandom(min, max){
      min = Math.ceil(min) * 100;
      max = Math.floor(max) * 100;
      return (Math.floor(Math.random() * (max - min + 1)) + min)/100;
    }
    static getIntRandom(min, max){
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    
    getHitPower() {
      let criticalHitChance = this.getRandom(1,2);
      let power = this.attack * criticalHitChance;
      return power;
    }
    getBlockPower(){
      let dodgeChance = this.getRandom(1,2);
      let power = this.attack * dodgeChance;
      return power;
    }
    isDead(){
      return this.health <= 0;
    }
  }
  
  export default Fighter;